<?php  namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model {
  protected $table = 'user'; // This model is using task table.

  protected $allowedFields = ['username','password','firstname','lastname']; // List of field for save method.

  public function check($username,$password) {
    $this->where('username',  $username);
    $query = $this->get();
    //print $this->getLastQuery(); // This might be used for debugging purposes to print out SQL clause.
    $row = $query->getRow();
    if ($row) { // Check if SQL returned a row.
      if (password_verify($password,$row->password)) { // Verify password.
        return $row;
      }
    }
    return null; // Null will be returned, if there is no user with given username and password.
  }
}