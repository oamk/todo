<?php namespace App\Controllers;

use App\Models\TodoModel;

class Todo extends BaseController {

  /**
   * Constructor starts session.
   */
  public function __construct() {
    $session = \Config\Services::session();
    $session->start();
  }

  public function index() {
    if (!isset($_SESSION['user'])) {
      return redirect('login');
    }
    $model = new TodoModel(); // Create object of TodoModel.
    $data['title'] = 'Todo'; // Set title.
    $data['todos'] = $model->getTodos(); // Retrieve information from database using model.
  
    echo view('templates/header',$data); // Set view and pass data.
  
    echo view('todo/list',$data);
    echo view('templates/footer',$data);
  }

  public function create() {   
    if (!isset($_SESSION['user'])) {
      return redirect('login');
    }
    $model = new TodoModel(); // Create object of TodoModel.

    if (!$this->validate([ // Validate and define rules inside square brackets.
        'title' => 'required|max_length[255]', // Title is required and maximum length is 255 (equals to database definition).
    ])){
        echo view('templates/header' , ['title' => 'Add new task']); // Pass title here (instead of using $data)
        echo view('todo/create');
        echo view('templates/footer');
    }
    else {
      $user = $_SESSION['user'];
      $model->save([ // Call model to save data. Read input values using correct names for database fields.
        'title' => $this->request->getVar('title'),
        'description' => $this->request->getVar('description'),
        'user_id' => $user->id
      ]);
      return redirect('todo'); // Redirect to todo route (list of todos).
    }
  }

  public function delete($id) {
    // Check if provided id is numeric (to prevent SQL injection).
    if (!is_numeric($id)) {
      throw new \Exception('Provided id is not an number.');
    }
    
    // Only logged user is allowed to delete.
    if (!isset($_SESSION['user'])) {
      return redirect('login');
    }
    $model = new TodoModel(); // Create object of TodoModel.
    
    $model->remove($id);
    return redirect('todo');
  }
}
